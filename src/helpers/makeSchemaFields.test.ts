import makeSchemaFields, { makeGraphQLType } from './makeSchemaFields';

const Types = {
  'STRING': 'String',
  'INTEGER': 'Int',
  'FLOAT': 'Float',
  'DATE': 'String',
  'BOOLEAN': 'Boolean',
};

describe('makeGraphQLType()', () => {
  it('should properly convert Sequelize types to GraphQL types', () => {
    Object.entries(Types).forEach(([sqlType, gqlType]) => {
      const sequelizeColumn = {
        gql: true,
        type: {
          key: sqlType,
        },
      };

      expect(makeGraphQLType(sequelizeColumn)).toEqual(gqlType);
    });
  });

  it('should respect a passed gql type', () => {
    Object.entries(Types).forEach(([sqlType, gqlType]) => {
      const sequelizeColumn = {
        gql: {
          type: 'someType'
        },
        type: {
          key: sqlType,
        },
      };

      expect(makeGraphQLType(sequelizeColumn)).toEqual('someType');
    });
  });

  it('should throw when presented with an invalid Sequelize type string', () => {
    const sequelizeColumn = {
      gql: true,
      type: {
        key: 'TYPE',
      },
    };

    expect(() => {
      makeGraphQLType(sequelizeColumn);
    }).toThrow();
  });

  it('should throw when presented with an invalid Sequelize type object', () => {
    const sequelizeColumn = {};

    expect(() => {
      makeGraphQLType(sequelizeColumn);
    }).toThrow();
  });
});

describe('makeSchemaFields()', () => {
  it('should properly convert a Sequelize model to GraphQL types list, excluding extraneous fields', () => {
    const sequelizeModel = {
      attributes: {
        id: {
          gql: true,
          type: {
            key: 'INTEGER',
          },
        },
        name: {
          gql: false,
          type: {
            key: 'STRING',
          },
        }
      }
    };

    expect(makeSchemaFields(sequelizeModel)).toMatchObject([
      {
        name: 'id',
        col: 'id',
        type: 'Int'
      }
    ]);
  });

  it('should respect GraphQL string alias', () => {
    const sequelizeModel = {
      attributes: {
        name: {
          gql: 'MyName',
          type: {
            key: 'STRING',
          },
        }
      }
    };

    expect(makeSchemaFields(sequelizeModel)).toMatchObject([
      {
        name: 'MyName',
        col: 'name',
        type: 'String'
      }
    ]);
  });

  it('should respect GraphQL options alias and type override', () => {
    const sequelizeModel = {
      attributes: {
        name: {
          gql: {
            name: 'MyName',
            type: 'SomeType',
          },
          type: {
            key: 'STRING',
          },
        }
      }
    };

    expect(makeSchemaFields(sequelizeModel)).toMatchObject([
      {
        name: 'MyName',
        col: 'name',
        type: 'SomeType'
      }
    ]);
  });
});