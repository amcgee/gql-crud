export const capitalize = (word: string): string => (
  word.charAt(0).toUpperCase() + word.substr(1)
);