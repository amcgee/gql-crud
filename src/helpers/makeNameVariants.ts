import { capitalize } from './util';
import { NameVariants } from '../types';

const makeNameVariants = ({ name, namePlural }: { name: string, namePlural?: string | null }): NameVariants => {
  const base = name.toLowerCase();
  const basePlural = namePlural
    ? namePlural.toLowerCase()
    : `${base}s`;
  const typeBase = capitalize(base);

  return {
    singular: base,
    plural: basePlural,
    Type: typeBase,
    TypePlural: capitalize(basePlural),
    FilterInputType: `${typeBase}FilterInput`,
    EditInputType: `${typeBase}EditInput`,
    CreateInputType: `${typeBase}CreateInput`
  };
};

export default makeNameVariants;
