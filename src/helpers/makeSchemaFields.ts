import { DefineAttributeColumnOptions, ColumnOptions } from 'sequelize';
import { SQLMappedField } from '..';

class SchemaFieldError extends Error {}

const TYPE_MAP = {
  'STRING': 'String',
  'INTEGER': 'Int',
  'FLOAT': 'Float',
  'DATE': 'String',
  'BOOLEAN': 'Boolean',
};

export const makeGraphQLType = (spec) => {
  if (!spec || !spec.type || !spec.type.key || !spec.gql) {
    throw new SchemaFieldError('Invalid Sequelize column object');
  }

  if (spec.gql.type) {
    return spec.gql.type;
  }

  const sqlType = spec.type.key;
  const gqlType = TYPE_MAP[sqlType];

  if (!gqlType) {
    throw new Error(`Unable to automatically convert SQL type '${sqlType}' to a GraphQL type, try using the 'gql.type' option `);
  }

  return gqlType;
};

const getGQLName = (spec, col) => {
  if (typeof spec.gql === 'string') {
    return spec.gql;
  } else if (spec.gql.name) {
    return spec.gql.name;
  } else {
    return col;
  }
};

function makeSchemaFields(Model: any): SQLMappedField[] {
  return Object.entries(Model.attributes)
    .filter(([col, spec]: [any, any]) => (
      spec.gql
    )).map(([col, spec]: [any, any]) => ({
      name: getGQLName(spec, col),
      col: col,
      type: makeGraphQLType(spec),

      readOnly: spec.gql.readOnly,
      createOnly: spec.gql.createOnly,
      editOnly: spec.gql.editOnly
    }));
}

export default makeSchemaFields;
