import defaultMakeFilterFields from './defaultMakeFilterFields';
import { SQLMappedField } from '..';

function makeField(name: string, type: string = 'String'): SQLMappedField {
  return {
    name,
    col: name,
    type
  };
}
describe('defaultMakeFilterFields', () => {
  it('Creates filter keys for String, Boolean, and Int types', () => {
    const fields = [
      makeField('name'),
      makeField('isAdmin', 'Boolean'),
      makeField('age', 'Int')
    ];
    const idField = makeField('id', 'ID');
    expect(defaultMakeFilterFields(fields, idField)).toMatchObject([
      { name: 'ids', type: '[ID]' },
      { name: 'name', type: 'String' },
      { name: 'name_in', type: '[String]' },
      { name: 'isAdmin', type: 'Boolean' },
      { name: 'age', type: 'Int' },
      { name: 'age_in', type: '[Int]' },
      { name: 'age_lt', type: 'Int' },
      { name: 'age_lte', type: 'Int' },
      { name: 'age_gt', type: 'Int' },
      { name: 'age_gte', type: 'Int' },
    ]);
  });
});