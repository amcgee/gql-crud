import makeNameVariants from './makeNameVariants';

it('should pluralize a simple input', () => {
  const variants = makeNameVariants({
    name: 'test'
  });
  expect(variants).toEqual({
    singular: 'test',
    plural: 'tests',
    Type: 'Test',
    TypePlural: 'Tests',
    FilterInputType: 'TestFilterInput',
    EditInputType: 'TestEditInput',
    CreateInputType: 'TestCreateInput'
  });
});

it('should correctly lowercase names and capitalize Type', () => {
  const variants = makeNameVariants({
    name: 'TEST'
  });
  expect(variants).toEqual({
    singular: 'test',
    plural: 'tests',
    Type: 'Test',
    TypePlural: 'Tests',
    FilterInputType: 'TestFilterInput',
    EditInputType: 'TestEditInput',
    CreateInputType: 'TestCreateInput'
  });
});

it('should respect a passed plural name', () => {
  const variants = makeNameVariants({
    name: 'test',
    namePlural: 'sometests'
  });
  expect(variants).toEqual({
    singular: 'test',
    plural: 'sometests',
    Type: 'Test',
    TypePlural: 'Sometests',
    FilterInputType: 'TestFilterInput',
    EditInputType: 'TestEditInput',
    CreateInputType: 'TestCreateInput'
  });
});