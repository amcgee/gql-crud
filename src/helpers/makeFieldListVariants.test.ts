import makeFieldListVariants from './makeFieldListVariants';

describe('makeFieldListVariants()', () => {
  it('Should properly parse unspecialized fields into a set of fields lists.', () => {
    const inputFields = [
      { name: 'id', col: 'id', type: 'ID' },
      { name: 'name', col: 'name', type: 'String' },
    ];

    expect(makeFieldListVariants(inputFields)).toMatchObject({
      idField: inputFields[0],
      readFields: inputFields,
      filterFields: [
        { name: 'ids', type: '[ID]' },
        { name: 'name', type: 'String' },
        { name: 'name_in', type: '[String]' }
      ],
      createFields: inputFields.filter(f => f.type !== 'ID'),
      editFields: inputFields.filter(f => f.type !== 'ID'),
    });
  });

  it('Should exclude readOnly fields from create and edit fields.', () => {
    const inputFields = [
      { name: 'id', col: 'id', type: 'ID' },
      { name: 'name', col: 'name', type: 'String', readOnly: true },
    ];

    expect(makeFieldListVariants(inputFields)).toMatchObject({
      idField: inputFields[0],
      readFields: inputFields,
      filterFields: [
        { name: 'ids', type: '[ID]' },
        { name: 'name', type: 'String' },
        { name: 'name_in', type: '[String]' }
      ],
      createFields: [],
      editFields: [],
    });
  });

  it('Should exclude createOnly fields from edit fields.', () => {
    const inputFields = [
      { name: 'id', col: 'id', type: 'ID' },
      { name: 'name', col: 'name', type: 'String', createOnly: true },
    ];

    expect(makeFieldListVariants(inputFields)).toMatchObject({
      idField: inputFields[0],
      readFields: inputFields,
      filterFields: [
        { name: 'ids', type: '[ID]' },
        { name: 'name', type: 'String' },
        { name: 'name_in', type: '[String]' }
      ],
      createFields: inputFields.filter(f => f.type !== 'ID'),
      editFields: [],
    });
  });

  it('Should exclude editOnly fields from create fields.', () => {
    const inputFields = [
      { name: 'id', col: 'id', type: 'ID' },
      { name: 'name', col: 'name', type: 'String', editOnly: true },
    ];

    expect(makeFieldListVariants(inputFields)).toMatchObject({
      idField: inputFields[0],
      readFields: inputFields,
      filterFields: [
        { name: 'ids', type: '[ID]' },
        { name: 'name', type: 'String' },
        { name: 'name_in', type: '[String]' }
      ],
      createFields: [],
      editFields: inputFields.filter(f => f.type !== 'ID'),
    });
  });

  it('Should exclude hidden fields from read and filter fields.', () => {
    const inputFields = [
      { name: 'id', col: 'id', type: 'ID' },
      { name: 'name', col: 'name', type: 'String', hidden: true },
    ];

    expect(makeFieldListVariants(inputFields)).toMatchObject({
      idField: inputFields[0],
      readFields: [inputFields[0]],
      filterFields: [
        { name: 'ids', type: '[ID]' }
      ],
      createFields: inputFields.filter(f => f.type !== 'ID'),
      editFields: inputFields.filter(f => f.type !== 'ID'),
    });
  });

  it('Should promote a passed ID field by name.', () => {
    const inputFields = [
      { name: 'id', col: 'id', type: 'Int' },
      { name: 'name', col: 'name', type: 'String', hidden: true },
    ];

    const mutatedIDField = { name: 'id', col: 'id', type: 'ID' };

    expect(makeFieldListVariants(inputFields, 'id')).toMatchObject({
      idField: mutatedIDField,
      readFields: [mutatedIDField],
      filterFields: [
        { name: 'ids', type: '[ID]' }
      ],
      createFields: [inputFields[1]],
      editFields: [inputFields[1]],
    });
  });

  it('Should throw when no ID field specified.', () => {
    const inputFields = [
      { name: 'name', col: 'name', type: 'String', hidden: true },
    ];

    expect(() => {
      makeFieldListVariants(inputFields);
    }).toThrow();
  });

  it('Should throw when multiple ID fields specified.', () => {
    const inputFields = [
      { name: 'id', col: 'id', type: 'ID' },
      { name: 'id2', col: 'id2', type: 'ID' },
      { name: 'name', col: 'name', type: 'String', hidden: true },
    ];

    expect(() => {
      makeFieldListVariants(inputFields);
    }).toThrow();
  });
});