import { Field, SQLMappedField, FieldListVariants } from '../types';
import makeFilterFields from './makeFilterFields';

function makeFieldListVariants(fields: SQLMappedField[], idField?: string): FieldListVariants {
  const mutatedFields = idField ? (
    fields.map(f => f.name === idField ? { ...f, type: 'ID' } : f)
  ) : fields.slice();

  const idFields = mutatedFields.filter(f => (f.type === 'ID'));
  if (idFields.length !== 1) {
    throw new Error('One and only one ID field must be specified');
  }
  const readFields = mutatedFields.filter(f => (!f.hidden));
  return {
    idField: idFields[0],

    readFields,
    filterFields: makeFilterFields(readFields),
    editFields: mutatedFields.filter(f => (f.type !== 'ID' && !f.readOnly && !f.createOnly && f)),
    createFields: mutatedFields.filter(f => (f.type !== 'ID' && !f.readOnly && !f.editOnly && f))
  };
}

export default makeFieldListVariants;