import { Field, SQLMappedField } from '../types';

const defaultMakeFilterFields = (fields: SQLMappedField[], idField: SQLMappedField): Field[] => (
  [{
    name: `${idField.name}s`, // ids
    type: `[${idField.type}]`,
  }].concat(fields.reduce((out: Field[], f) => {
    switch (f.type) {
      case 'String':
        out.push({
          name: f.name,
          type: f.type,
        }, {
          name: `${f.name}_in`,
          type: '[String]',
        });
        break;
      case 'Boolean':
        out.push({
          name: f.name,
          type: f.type,
        });
        break;
      case 'Int':
        out.push({
          name: f.name,
          type: 'Int',
        }, {
          name: `${f.name}_in`,
          type: '[Int]',
        }, {
          name: `${f.name}_lt`,
          type: 'Int',
        }, {
          name: `${f.name}_lte`,
          type: 'Int',
        }, {
          name: `${f.name}_gt`,
          type: 'Int',
        }, {
          name: `${f.name}_gte`,
          type: 'Int',
        });
        break;
    }
    return out;
  }, []))
);

export default defaultMakeFilterFields;
