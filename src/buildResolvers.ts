import makeSequelizeEngine from './engines/sequelize';
import { NameVariants, MakeEngineOptions, BuildResolversArgs } from './types';

const identityFunction = (f) => f;

function buildResolvers(options: BuildResolversArgs): any {
  const { names, readOnly, authWrap = identityFunction, engine = makeSequelizeEngine(options) } = options;

  const resolvers = {
    Query: {},
    Mutation: {}
  };
  resolvers.Query = {
    [names.Type]: authWrap(engine.GET),
    [`all${names.TypePlural}`]: authWrap(engine.GET_ALL),
    [`_all${names.TypePlural}Meta`]: authWrap(engine.GET_ALL_META),
  };
  if (!readOnly) {
    resolvers.Mutation = {
      [`create${names.Type}`]: authWrap(engine.CREATE),
      [`update${names.Type}`]: authWrap(engine.UPDATE),
      [`delete${names.Type}`]: authWrap(engine.DELETE),
    };
  }

  return resolvers;
}

export default buildResolvers;