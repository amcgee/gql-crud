import buildTypeDefs from './buildTypeDefs';
import makeFieldListVariants from './helpers/makeFieldListVariants';
import { SQLMappedField } from './types';

const sampleNames = {
  singular: 'test',
  plural: 'tests',
  Type: 'Test',
  TypePlural: 'Tests',
  FilterInputType: 'TestFilterInput',
  EditInputType: 'TestEditInput',
  CreateInputType: 'TestCreateInput'
};

function makeField(name: string, type: string = 'String'): SQLMappedField {
  return {
    name,
    col: name,
    type: type
  };
}
describe('buildTypeDefs()', () => {
  it('should create a simple type with Query types', () => {
    const fieldVariants = makeFieldListVariants([makeField('id', 'ID'), makeField('name')]);
    const typeDefs = buildTypeDefs({
      names: sampleNames,
      readOnly: false,
      ...fieldVariants,
    });

    // Query and Mutation types
    expect(typeDefs.match(/type Query {\n\s+Test\(id: ID!\): Test\n\s+allTests\(.*\): \[Test\]\n\s+_allTestsMeta\(.*\): ListMetadata\n\s+}/g)).not.toBeNull();
    expect(typeDefs.match(/type Mutation {\n\s+createTest\(data: TestCreateInput!\): Test\n\s+updateTest\(id: ID!, data: TestEditInput!\): Test\n\s+deleteTest\(id: ID!\): Boolean\n\s+}/g)).not.toBeNull();

    // Non-empty types
    expect(typeDefs.match(/type Test {\s+}/g)).toBeNull();
    expect(typeDefs.match(/input TestFilterInput {\s+}/g)).toBeNull();
    expect(typeDefs.match(/input TestEditInput {\s+}/g)).toBeNull();

    expect(typeDefs).toMatchSnapshot();
  });

  it('should create a simple type with NO filterable fields', () => {
    const fieldVariants = makeFieldListVariants([makeField('id', 'ID'), makeField('name')]);
    const typeDefs = buildTypeDefs({
      names: sampleNames,
      readOnly: false,
      ...fieldVariants,
      filterFields: [],
      idField: makeField('id', 'Int')
    });

    // Non-empty types
    expect(typeDefs.match(/type Test {\s+}/g)).toBeNull();
    expect(typeDefs.match(/input TestEditInput {\s+}/g)).toBeNull();

    // Empty filter type
    expect(typeDefs.match(/input TestFilterInput {\s+}/g)).not.toBeNull();

    expect(typeDefs).toMatchSnapshot();
  });
});