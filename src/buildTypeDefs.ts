import { NameVariants, SQLMappedField, Field, FieldListVariants } from './types';

// TODO: Split Edit and Create input types
//       (some create fields need to be non-null)
interface BuildTypeDefsArgs extends FieldListVariants {
  names: NameVariants;
  readOnly: boolean;
  filterFields: Field[];
  idField: SQLMappedField;
}

function makeTypeDef(name, isInputType, fields) {
  const typeLabel = isInputType ? 'input' : 'type';
  return (
`    ${typeLabel} ${name} {
${fields.map(f => (
`      ${f.name}: ${f.type}\n`
)).join('')
}    }`);
}

function buildTypeDefs({ names, readOnly, readFields, filterFields, editFields, createFields, idField}: BuildTypeDefsArgs): string {
  const typeDefs = `
${makeTypeDef(names.Type, false, readFields)}
    type Query {
      ${names.Type}(${idField.name}: ID!): ${names.Type}
      all${names.TypePlural}(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: ${names.FilterInputType}): [${names.Type}]
      _all${names.TypePlural}Meta(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: ${names.FilterInputType}): ListMetadata
    }
${makeTypeDef(names.FilterInputType, true, filterFields)}
${readOnly ? '' : (`
    type Mutation {
      create${names.Type}(data: ${names.CreateInputType}!): ${names.Type}
      update${names.Type}(${idField.name}: ID!, data: ${names.EditInputType}!): ${names.Type}
      delete${names.Type}(${idField.name}: ID!): Boolean
    }
${makeTypeDef(names.EditInputType, true, editFields)}
${makeTypeDef(names.CreateInputType, true, createFields)}
`)}`;

  return typeDefs;
}

export default buildTypeDefs;