import buildTypeDefs from './buildTypeDefs';
import buildResolvers from './buildResolvers';

import makeFieldListVariants from './helpers/makeFieldListVariants';
import makeSchemaFields from './helpers/makeSchemaFields';
import makeNameVariants from './helpers/makeNameVariants';

import { capitalize } from './helpers/util';
import { Field, NameVariants, SQLMappedField, BuildSchemaArgs, Schema } from './types';

export const buildSchema: (args: BuildSchemaArgs) => Schema = (args) => {
  const {
    name,
    namePlural,
    DBModel,
    idField,
    readOnly = false,
    authWrap
  } = args;

  const names = makeNameVariants({ name, namePlural });
  const fields = makeSchemaFields(DBModel);

  const fieldVariants = makeFieldListVariants(fields, idField);

  return {
    name: names.Type,
    typeDefs: buildTypeDefs({ names, readOnly, ...fieldVariants }),
    resolvers: buildResolvers({ names, readOnly, DBModel, authWrap, ...fieldVariants }),
  };
};

export default buildSchema;