import buildResolvers from './buildResolvers';
import { SQLMappedField, Engine } from './types';
import { Model } from 'sequelize';

const sampleNames = {
  singular: 'test',
  plural: 'tests',
  Type: 'Test',
  TypePlural: 'Tests',
  FilterInputType: 'TestFilterInput',
  EditInputType: 'TestEditInput',
  CreateInputType: 'TestCreateInput',
};

const mockModel = <Model<any, any>>{};
const mockEngine = {
  GET: jest.fn().mockResolvedValue({}),
  GET_ALL: jest.fn().mockResolvedValue({}),
  GET_ALL_META: jest.fn().mockResolvedValue({}),
  CREATE: jest.fn().mockResolvedValue({}),
  UPDATE: jest.fn().mockResolvedValue({}),
  DELETE: jest.fn().mockResolvedValue({}),
};

function makeField(name: string, type: string = 'String'): SQLMappedField {
  return {
    name,
    col: name,
    type: type
  };
}

describe('buildResolvers()', () => {
  it('should build a resolver tree with functions at all required nodes.', () => {
    const resolvers = buildResolvers({
      names: sampleNames,
      readOnly: false,
      DBModel: mockModel,

      idField: makeField('id', 'Int'),
      readFields: [],
      filterFields: [],
      editFields: [],
      createFields: [],
    });

    expect(resolvers).toHaveProperty(['Query', 'Test']);
    expect(resolvers).toHaveProperty(['Query', 'allTests']);
    expect(resolvers).toHaveProperty(['Query', '_allTestsMeta']);

    expect(resolvers).toHaveProperty(['Mutation', 'createTest']);
    expect(resolvers).toHaveProperty(['Mutation', 'updateTest']);
    expect(resolvers).toHaveProperty(['Mutation', 'deleteTest']);
  });

  it('Should respect readonly flag.', () => {
    const resolvers = buildResolvers({
      names: sampleNames,
      readOnly: true,
      DBModel: mockModel,

      idField: makeField('id', 'Int'),
      readFields: [],
      filterFields: [],
      editFields: [],
      createFields: [],
    });

    expect(resolvers).toHaveProperty(['Query', 'Test']);
    expect(resolvers).toHaveProperty(['Query', 'allTests']);
    expect(resolvers).toHaveProperty(['Query', '_allTestsMeta']);

    expect(resolvers).not.toHaveProperty(['Mutation', 'createTest']);
    expect(resolvers).not.toHaveProperty(['Mutation', 'updateTest']);
    expect(resolvers).not.toHaveProperty(['Mutation', 'deleteTest']);
  });

  it('should properly use the passed engine for all queries and mutations.', () => {
    jest.clearAllMocks();

    const mockAuthWrap = jest.fn().mockImplementation(f => f);
    const resolvers = buildResolvers({
      names: sampleNames,
      readOnly: false,
      engine: mockEngine,
      DBModel: mockModel,

      idField: makeField('id', 'Int'),
      readFields: [],
      filterFields: [],
      editFields: [],
      createFields: [],
    });

    resolvers.Query.Test();
    expect(mockEngine.GET).toHaveBeenCalled();

    resolvers.Query.allTests();
    expect(mockEngine.GET_ALL).toHaveBeenCalled();

    resolvers.Query._allTestsMeta();
    expect(mockEngine.GET_ALL_META).toHaveBeenCalled();

    resolvers.Mutation.createTest();
    expect(mockEngine.CREATE).toHaveBeenCalled();

    resolvers.Mutation.updateTest();
    expect(mockEngine.UPDATE).toHaveBeenCalled();

    resolvers.Mutation.deleteTest();
    expect(mockEngine.DELETE).toHaveBeenCalled();
  });

  it('should properly wrap the resolvers with a passed authWrap function.', () => {
    jest.clearAllMocks();

    const mockAuthFn = jest.fn().mockImplementation((obj, args, ctx, info) => {});

    const mockAuthWrap = jest.fn().mockImplementation(f => mockAuthFn);
    const resolvers = buildResolvers({
      names: sampleNames,
      readOnly: false,
      engine: mockEngine,
      DBModel: mockModel,
      authWrap: mockAuthWrap,

      idField: makeField('id', 'Int'),
      readFields: [],
      filterFields: [],
      editFields: [],
      createFields: [],
    });

    expect(mockAuthWrap).toHaveBeenCalledTimes(6);

    resolvers.Query.allTests();
    resolvers.Query._allTestsMeta();
    resolvers.Query.Test();
    resolvers.Mutation.createTest();
    resolvers.Mutation.updateTest();
    resolvers.Mutation.deleteTest();

    expect(mockAuthFn).toHaveBeenCalledTimes(6);
  });
});