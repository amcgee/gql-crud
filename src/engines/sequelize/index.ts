import * as Sequelize from 'sequelize';

import getSequelizeAttributes from './helpers/getSequelizeAttributes';
import getQueryModifiers from './helpers/getQueryModifiers';
import buildFilterWhere from './helpers/buildFilterWhere';
import { Field, NameVariants, MakeEngineOptions, Engine } from '../../types';
import { GraphQLResolveInfo } from 'graphql';

function makeSequelizeEngine({ names, readFields, filterFields, createFields, editFields, DBModel, idField }: MakeEngineOptions): Engine {
  return {
    GET: (root, args, context, info: GraphQLResolveInfo) => {
      return DBModel.findOne({
        where: {
          [idField.col]: args[idField.name]
        },
        attributes: getSequelizeAttributes(info, readFields)
      }).then(res => res.dataValues);
    },
    GET_ALL: (root, args, context, info: GraphQLResolveInfo) => {
      const queryArgs = getQueryModifiers(args, idField);
      return DBModel.findAll({
        ...queryArgs,
        attributes: getSequelizeAttributes(info, readFields),
      }).then(res => {
        return res.map(r => r.dataValues); // TODO: This shouldn't be necessary but for some reason it is, otherwise we don't get column name aliasing.
      });
    },
    GET_ALL_META: (root, args, context, info: GraphQLResolveInfo) => {
      return DBModel.findAll({
        where: buildFilterWhere(args.filter, idField),
        attributes: [[Sequelize.fn('COUNT', Sequelize.col('*')), 'count']]
      }).then(res => {
        return {
          count: res[0].dataValues.count
        };
      });
    },
    CREATE: (root, args, context, info: GraphQLResolveInfo) => {
      return DBModel.create(args.data)
        .then(res => {
          return DBModel.find({
            where: {
              [idField.col]: res.dataValues[idField.col]
            },
            attributes: getSequelizeAttributes(info, readFields),
          }).then(res => res.dataValues);
        });
    },
    UPDATE: (root, args, context, info: GraphQLResolveInfo) => {
      return DBModel.update(args.data, {
        where: {
          [idField.col]: args[idField.name]
        }
      }).then(res => {
        return DBModel.find({
          where: {
            [idField.col]: args[idField.name]
          },
          attributes: getSequelizeAttributes(info, readFields),
        }).then(res => res.dataValues);
      });
    },
    DELETE: (root, args, context, info: GraphQLResolveInfo) => {
      return DBModel.destroy({
        where: {
          [idField.col]: args[idField.name]
        },
      });
    }
  };
}

export default makeSequelizeEngine;
