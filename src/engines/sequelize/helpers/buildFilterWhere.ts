import { Op } from 'sequelize';
import { Field, SQLMappedField } from '../../../types';
import * as Sequelize from 'sequelize';
import { FilterArgs } from '../types';

function buildFilterWhere(filter: FilterArgs = {}, idField: SQLMappedField): Sequelize.WhereOptions<any> {
  const where = {};

  Object.keys(filter).forEach(f => {
    if (f === `${idField.name}s`) { // This should really be `id_in` but we use `ids` to support the `graphql-simple` client.
      const field = idField.name;
      where[field] = {
        ...where[field],
        [Op.in]: filter[f],
      };
    } else if (f.endsWith('_in')) {
      const field = f.slice(0, -3);
      where[field] = {
        ...where[field],
        [Op.in]: filter[f],
      };
    } else if (f.endsWith('_lt')) {
      const field = f.slice(0, -3);
      where[field] = {
        ...where[field],
        [Op.lt]: filter[f]
      };
    } else if (f.endsWith('_lte')) {
      const field = f.slice(0, -4);
      where[field] = {
        ...where[field],
        [Op.lte]: filter[f]
      };
    } else if (f.endsWith('_gt')) {
      const field = f.slice(0, -3);
      where[field] = {
        ...where[field],
        [Op.gt]: filter[f]
      };
    } else if (f.endsWith('_gte')) {
      const field = f.slice(0, -4);
      where[field] = {
        ...where[field],
        [Op.gte]: filter[f]
      };
    } else if (f.endsWith('_ne')) {
      const field = f.slice(0, -3);
      where[field] = {
        [Op.ne]: filter[f]
      };
    } else {
      where[f] = filter[f];
    }
  });

  return where;
}

export default buildFilterWhere;