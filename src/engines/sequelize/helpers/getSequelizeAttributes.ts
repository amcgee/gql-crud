import { Field, SQLMappedField } from '../../../types';
import { GraphQLResolveInfo, InlineFragmentNode, FieldNode } from 'graphql';

type SequelizeColumnDefinition = string | [string, string];
type GetSequelizeAttributesFunction = (info: GraphQLResolveInfo, fields: SQLMappedField[]) => SequelizeColumnDefinition[];

const getSequelizeAttributes: GetSequelizeAttributesFunction = (info, fields) => {
  if (!info.fieldNodes || !info.fieldNodes[0] || !info.fieldNodes[0].selectionSet) {
    return [];
  }

  const selections = info.fieldNodes[0].selectionSet.selections;
  return selections.filter(selection => (
    (<FieldNode>selection).name && (<FieldNode>selection).name.value
  )).map((gqlField: FieldNode) => {
    const field = fields.find(f => f.name === gqlField.name.value);
    if (!field) {
      // This should never happen, so if it does throw an error.
      throw new Error(`Unrecognized column '${gqlField.name.value}' requested.`);
    }

    const col: SequelizeColumnDefinition = field.name === field.col ? field.col : [field.col, field.name];
    return col;
  });
};

export default getSequelizeAttributes;
