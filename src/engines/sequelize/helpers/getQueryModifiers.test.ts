import getQueryModifiers from './getQueryModifiers';
import { SQLMappedField } from '../../..';

const idField: SQLMappedField = {
  name: 'id',
  col: 'id',
  type: 'Int'
};
describe('SEQUELIZE::getQueryModifiers()', () => {
  it('Parses limit, offset, order, and where correctly.', () => {
    const args = {
      perPage: 10,
      page: 3,
      sortField: 'field',
      filter: {}
    };

    expect(getQueryModifiers(args, idField)).toMatchObject({
      limit: 10,
      offset: 30,
      order: ['field'],
      where: {},
    });
  });

  it('Correctly detects DESC sort order', () => {
    const args = {
      perPage: 10,
      page: 3,
      sortField: 'field',
      sortOrder: 'desc',
      filter: {}
    };

    expect(getQueryModifiers(args, idField)).toMatchObject({
      limit: 10,
      offset: 30,
      order: [['field', 'DESC']],
      where: {},
    });
  });

  it('Case-insensitive desc sort order', () => {
    const args = {
      perPage: 10,
      page: 3,
      sortField: 'field',
      sortOrder: 'DeSc',
      filter: {}
    };

    expect(getQueryModifiers(args, idField)).toMatchObject({
      limit: 10,
      offset: 30,
      order: [['field', 'DESC']],
      where: {},
    });
  });
});