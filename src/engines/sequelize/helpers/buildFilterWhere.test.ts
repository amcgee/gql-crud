import buildFilterWhere from './buildFilterWhere';
import { SQLMappedField } from '../../..';
import { Op } from 'sequelize';

const idField: SQLMappedField = {
  name: 'id',
  col: 'id',
  type: 'String'
};

describe('SEQUELIZE::buildFilterWhere()', () => {
  it('ids: Should return a simple id list where clause', () => {
    const ids = [3, 4, 5];
    const filter = {
      'ids': ids
    };

    expect(buildFilterWhere(filter, idField)).toMatchObject({
      'id': {
        [Op.in]: ids
      }
    });
  });

  it('Should return a simple \'in\' list where clause', () => {
    const names = ['Peter', 'Paul', 'Mary'];
    const filter = {
      'name_in': names
    };

    expect(buildFilterWhere(filter, idField)).toMatchObject({
      'name': {
        [Op.in]: names
      }
    });
  });

  it('Should return a simple equality condition where clause', () => {
    const filter = {
      'name': 'Bob'
    };

    expect(buildFilterWhere(filter, idField)).toMatchObject({
      'name': 'Bob'
    });
  });

  it('Should return a simple inequality condition where clause', () => {
    const filter = {
      'name_ne': 'Bob'
    };

    expect(buildFilterWhere(filter, idField)).toMatchObject({
      'name': {
        [Op.ne]: 'Bob'
      }
    });
  });

  it('Should return a simple int LT condition where clause', () => {
    const filter = {
      'age_lt': 26
    };

    expect(buildFilterWhere(filter, idField)).toMatchObject({
      'age': {
        [Op.lt]: 26
      }
    });
  });

  it('Should return a simple int LTE condition where clause', () => {
    const filter = {
      'age_lte': 26
    };

    expect(buildFilterWhere(filter, idField)).toMatchObject({
      'age': {
        [Op.lte]: 26
      }
    });
  });
  it('Should return a simple int GT condition where clause', () => {
    const filter = {
      'age_gt': 26
    };

    expect(buildFilterWhere(filter, idField)).toMatchObject({
      'age': {
        [Op.gt]: 26
      }
    });
  });
  it('Should return a simple int GTE condition where clause', () => {
    const filter = {
      'age_gte': 26
    };

    expect(buildFilterWhere(filter, idField)).toMatchObject({
      'age': {
        [Op.gte]: 26
      }
    });
  });
});