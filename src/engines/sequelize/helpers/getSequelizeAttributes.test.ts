import getSequelizeAttributes from './getSequelizeAttributes';
import { SQLMappedField } from '../../..';
import { GraphQLResolveInfo } from 'graphql';

const sampleFields: SQLMappedField[] = [{
  name: 'name',
  col: 'name',
  type: 'String',
}, {
  name: 'age',
  col: 'personAge',
  type: 'Int',
}];
describe('SEQUELIZE::getSequelizeAttributes()', () => {
  it('Selects fields based on GraphQL info', () => {
    const info: any = {
      fieldNodes: [{
        selectionSet: {
          selections: [{
            name: {
              value: 'name'
            }
          }]
        }
      }]
    };

    const attrs = getSequelizeAttributes(info, sampleFields);
    expect(attrs).toMatchObject([
      'name'
    ]);
  });

  it('Return an empty list when an invalid info is passed', () => {
    const info: any = {
      fieldNodes: []
    };

    const attrs = getSequelizeAttributes(info, sampleFields);
    expect(attrs).toMatchObject([]);
  });

  it('Correctly selects fields with aliases', () => {
    const info: any = {
      fieldNodes: [{
        selectionSet: {
          selections: [{
            name: {
              value: 'name'
            }
          }, {
            name: {
              value: 'age'
            }
          }]
        }
      }]
    };

    const attrs = getSequelizeAttributes(info, sampleFields);
    expect(attrs).toMatchObject([
      'name',
      ['personAge', 'age']
    ]);
  });

  it('Throw an error if an unrecognized column is requested', () => {
    const info: any = {
      fieldNodes: [{
        selectionSet: {
          selections: [{
            name: {
              value: 'address'
            }
          }]
        }
      }]
    };

    expect(() => {
      getSequelizeAttributes(info, sampleFields);
    }).toThrow();
  });
});