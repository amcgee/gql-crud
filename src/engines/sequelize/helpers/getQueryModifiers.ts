import buildFilterWhere from './buildFilterWhere';
import { Field, SQLMappedField } from '../../../types';
import * as Sequelize from 'sequelize';
import { FindAllArgs } from '../types';

const defaultLimit = 100;

type SequelizeSortOrder = (string | [string, string])[];
type QueryModifiers = {
  limit?: number
  offset?: number
  order?: SequelizeSortOrder
  where: Sequelize.WhereOptions<any>
};

function getQueryModifiers(args: FindAllArgs, idField: SQLMappedField): QueryModifiers {
  const limit = args.perPage || defaultLimit;
  const offset = args.page ? limit * args.page : 0;

  // TODO: SANITIZE FIELD NAME
  const descending = args.sortOrder && args.sortOrder.toLowerCase() === 'desc';
  const order: SequelizeSortOrder = args.sortField && [
    descending
      ? [ args.sortField, 'DESC' ]
      : args.sortField
  ];
  const where = buildFilterWhere(args.filter, idField);

  return { limit, offset, order, where };
}

export default getQueryModifiers;
