import { NameVariants, Field, SQLMappedField } from '../../types';
import * as Sequelize from 'sequelize';

export type FilterArgs = {
  [filterKey: string]: (number | string | boolean) | (number | string | boolean)[]
};
export type FindAllArgs = {
  page?: number,
  perPage?: number,
  sortField?: string,
  sortOrder?: string,
  filter: FilterArgs
};