import buildSchema from './buildSchema';
import { Model } from 'sequelize';
import * as Sequelize from 'sequelize';

const mockModel = (new Sequelize('db', 'u', 'p', { dialect: 'sqlite' })).define('user', {
  id: <Sequelize.DefineAttributeColumnOptions>{
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
    gql: {
      type: 'ID'
    }
  },
  name: <Sequelize.DefineAttributeColumnOptions>{
    type: Sequelize.STRING,
    gql: true
  },
  color: <Sequelize.DefineAttributeColumnOptions>{
    type: Sequelize.STRING,
    gql: 'favoriteColor'
  }
}, {
  timestamps: false
});

describe('buildSchema()', () => {
  it('Should succeed in creating a consistent schema definition', () => {
    let schema;
    expect(() => {
      schema = buildSchema({
        name: 'test',
        DBModel: mockModel,
      });
    }).not.toThrow();

    expect(schema).toMatchSnapshot();
  });

  it('Should succeed in creating a consistent readOnly schema definition', () => {
    let schema;
    expect(() => {
      schema = buildSchema({
        name: 'test',
        readOnly: true,
        DBModel: mockModel,
      });
    }).not.toThrow();

    expect(schema).toMatchSnapshot();
  });
});