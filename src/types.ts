import * as Sequelize from 'sequelize';
import { GraphQLResolveInfo } from 'graphql';

export type SQLMappedField = {
  name: string
  col: string
  type: string

  hidden?: boolean
  readOnly?: boolean
  createOnly?: boolean
  editOnly?: boolean
};

export type Field = {
  name: string
  type: string
};

export type FieldListVariants = {
  idField: SQLMappedField,

  readFields: SQLMappedField[],
  editFields: SQLMappedField[],
  createFields: SQLMappedField[],

  filterFields: Field[],
};

export type NameVariants = {
  singular: string
  plural: string
  Type: string
  TypePlural: string
  FilterInputType: string
  EditInputType: string
  CreateInputType: string
};

export type ResolverFunction = (root: any, args: any, ctx: any, info: GraphQLResolveInfo) => any;
export type AuthWrapFunction = (f: ResolverFunction) => ResolverFunction;

export interface MakeEngineOptions extends FieldListVariants {
  names: NameVariants;
  DBModel: Sequelize.Model < any, any >;
}

export type EngineResolverFunction = (root, args, context, info: GraphQLResolveInfo) => any;
export type Engine = {
  GET: EngineResolverFunction
  GET_ALL: EngineResolverFunction
  GET_ALL_META: EngineResolverFunction
  CREATE: EngineResolverFunction
  UPDATE: EngineResolverFunction
  DELETE: EngineResolverFunction
};

export interface BuildResolversArgs extends MakeEngineOptions {
  engine?: Engine;
  readOnly: boolean;
  authWrap?: AuthWrapFunction;
}

export type BuildSchemaArgs = {
  name: string;
  namePlural?: string;
  DBModel: Sequelize.Model < any, any >;
  idField?: string;
  authWrap?: AuthWrapFunction;
  readOnly?: boolean;
};

export type Schema = {
  name: string
  typeDefs: string
  resolvers: {
    [key: string]: any
  }
};