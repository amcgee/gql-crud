# GQL-CRUD

[![pipeline status](https://gitlab.com/amcgee/gql-crud/badges/master/pipeline.svg)](https://gitlab.com/amcgee/gql-crud/commits/master)
[![test coverage](https://gitlab.com/amcgee/gql-crud/badges/master/coverage.svg)](https://gitlab.com/amcgee/gql-crud/commits/master)

A code sample work-in-progess module for GraphQL to SQL translation, written in typescript for node.js.

## Installation

```bash
yarn install
```

## Running Test Suite

```bash
yarn test
```

## Basic Example

```bash
yarn run start-example
```

GraphQL server will be running at `localhost:4000`, a playground for interactive querying available at `localhost:4000/playground`.
