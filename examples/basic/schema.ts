import * as Sequelize from 'sequelize';

import { buildSchema } from '../../src';
import { makeExecutableSchema } from 'graphql-tools';

const makeSchema = (db) => {
  const UserModel = {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
      gql: {
        type: 'ID'
      },
    },

    name: { type: Sequelize.STRING, gql: true },
  };

  db.define('user', UserModel, {
    timestamps: false,
  });

  db.sync({ force: true });

  const { typeDefs, resolvers } = buildSchema({
    name: 'User',
    DBModel: db.models.user,
  });

  const commonTypeDefs = `
    type ListMetadata {
      count: Int!
    }
    scalar Date
  `;

  return makeExecutableSchema({ typeDefs: [ typeDefs, commonTypeDefs ], resolvers });
};

export default makeSchema;