import { graphql } from 'graphql';

import makeSchema from './schema';
import * as Sequelize from 'sequelize';

import * as fs from 'fs';

const dbFile = `./${Math.floor(Math.random() * 9000 + 1000)}.test.db`;
const db = new Sequelize('database', 'username', 'password', {
  dialect: 'sqlite',
  storage: dbFile,
});

const schema = makeSchema(db);

beforeAll(async () => {
  await db.sync({
    force: true
  });
});

describe('Basic end-to-end Schema test', () => {
  it('Should return an empty array of Users and a total user count of 0', async () => {
    const query = `
      query {
        allUsers {
          id
          name
        }
        _allUsersMeta {
          count
        }
      }
    `;
    const result = await graphql(schema, query);

    expect(result).toMatchObject({
      data: {
        allUsers: [],
        _allUsersMeta: {
          count: 0
        }
      }
    });
  });

  let userID;
  const name = 'TEST';

  it('Should create a new user and return the user ID and name', async () => {
    const query = `
      mutation {
        createUser(data:{name:"${name}"}) {
          id
          name
        }
      }
    `;
    const result = await graphql(schema, query);

    expect(result.errors).toBeUndefined();

    expect(result.data.createUser.id).not.toBeUndefined();
    expect(result.data.createUser.name).toBe(name);

    userID = result.data.createUser.id;
  });

  it('Should return the single existing user and a total user count of 1', async () => {
    const query = `
      query {
        allUsers {
          id
          name
        }
        _allUsersMeta {
          count
        }
      }
    `;
    const result = await graphql(schema, query);

    expect(result).toMatchObject({
      data: {
        allUsers: [{
          id: userID,
          name: name
        }],
        _allUsersMeta: {
          count: 1
        }
      }
    });
  });

  it('Should return the user by id', async () => {
    const query = `
      query {
        User(id:${userID}) {
          id
          name
        }
      }
    `;
    const result = await graphql(schema, query);

    expect(result).toMatchObject({
      data: {
        User: {
          id: userID,
          name: name
        }
      }
    });
  });

  it('Should update the user\'s name', async () => {
    const newName = 'BATMAN';
    const query = `
      mutation {
        updateUser(id:${userID}, data: {name: "${newName}"}) {
          name
        }
      }
    `;
    const result = await graphql(schema, query);

    expect(result).toMatchObject({
      data: {
        updateUser: {
          name: newName
        }
      }
    });
  });

  it('Should delete the user', async () => {
    const query = `
      mutation {
        deleteUser(id:${userID})
      }
    `;
    const result = await graphql(schema, query);

    expect(result).toMatchObject({
      data: {
        deleteUser: true
      }
    });
  });
});

afterAll(() => {
  fs.unlinkSync(dbFile);
  console.log(`Deleted test sqlite database file ${dbFile}`);
});