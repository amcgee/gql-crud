import { GraphQLServer } from 'graphql-yoga';
import * as Sequelize from 'sequelize';
import makeSchema from './schema';

export const db = new Sequelize('database', 'username', 'password', {
  dialect: 'sqlite',
  storage: './example.db',
});

const createContext = req => ({});
const server = new GraphQLServer({
  schema: makeSchema(db),
  context: createContext
});

const SERVER_PORT = 4000;
server.start({
  port: SERVER_PORT,
  playground: '/playground',
}, () => {
  console.log(`NAYEN GraphQL Server is running on http://localhost:${SERVER_PORT}`);
  console.log(`GraphQL Playground is running on http://localhost:${SERVER_PORT}/playground`);
});
